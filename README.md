# Multilang-manual

## About

This project is a set of LaTeX files and a workflow 
to setup and maintain a multilingual manual in an
environment which does not have a dedicated department/workforce for documentation.
An example are technical usermanuals in a University with diverse language (i.e. 
not all users necessarily speak the same language).

The underlying concept should be also easily adaptable to other LaTeX-based
documents where language or format is selected at compile time.


## Background

Writing a documentation seems easy. At least at first glance. However, to do
this sustainably and with minimal effort is actually not such an easy task. If
one does this by himself, it might be possible to get away with a Word file on
the local hard disk. But sharing this manual with others, and making sure
everyone has access to the latest version immediately involves a lot of
communication overhead.

But what if you need the input from others to write the documents? What if
a single document requires input from several people because of the size? If
these contributors send their files where they edited parts of a word file,
merging without losing any data becomes a tedious and time consuming task.

Adding another language will be finally the point where it will not be possible
anymore to manage the manuals and delegate corrections/translations in your
spare time. Keeping the two language versions in sync (in terms of information
content and accuracy) and merging contributor input becomes an impossible task.

This problem is definitely not new. Others had similar problems and devised
their own solutions. For large organizations, this inevitably leads to a system
that relies on a databased-backend to store the information objects (with
version control), and some description file that defines which data has to be
retrieved from the database to assemble the final output. These systems are
commonly referred to as [Document Management
System](https://en.wikipedia.org/wiki/Document_management_system).  Or the data
is stored as xml, for example [S1000D](https://en.wikipedia.org/wiki/S1000D).
While these systems are very extensible, they are expensive and require
considerable manpower to maintain.  One can not expect that a contributor or
translator directly interfaces with the database objects or xml files without
messing with the format.

With the power of LaTeX (for beautiful typesetting) and git (for version control),
all of this is possible with the power of open source and relatively small amount of
work (except writing the actual documentation, that is).

### Concept of multilang-manual

Each manual consists of one ``.tex`` file and any number of additional image files.
The ``.tex`` file is a standard LaTeX source file with a preamble, and the ``\begin{document}...\end{document}`` environment. The files should be utf-8 encoded.

To allow language selection during compile time, the text that
is changed according to language is enclosed in a custom macro: ``\langxx{This is the text}``.
``xx`` can be any number of characters, but in the example documentation here the
language code is used. For example ``\langen{}`` for English or ``\langja{}``
for Japanese.

Furthermore, a custom ``multilangmanual.cls`` file (Class file) is used that
allows format selection, namely for screen reading and printing (A4), or for smartphones (minimal margins, larger text).
The class file can be modified to include branding or some additional settings.

A minimal source file with English and Japanese text could look like this:

```latex
\documentclass[BCOR=15mm,DIV=calc,index=0]{multilangmanual}

% This should be repeated for each language
\providecommand{\langen}[1]{%
    \ifen#1\fi}
\providecommand{\langja}[1]{%
    \ifja#1\fi}

\begin{document}

\begin{center}
{\usekomafont{title}\huge\noindent
\langja{Manual\\} %%%%
\langen{マニュアル}~\\[0.5em]}
\end{center}

\langen{Welcome}
\langja{ようこそ}

\end{document}
```

To compile this into a ``.pdf`` for screen reading with both languages, do one of the following:

#### MikTeX

For windows users, [MikTeX](https://miktex.org) should be the preferred LaTeX distribution.
After installation open TeXWorks, and make a new Tool configuration by going to Edit -->
Preferences --> Typesetting and press the blue + to add a new Processing Tool.
In the Tool Configuration, give a name (such as ``Manual print all Lang``), and
type ``xelatex.exe`` for Program. XeLaTeX is preferred over PdfLaTeX when CJK (China, Japan, Korea) languages have to be typeset. In the Arguments part, add the following four lines:

```
$synctexoption
-jobname=$basename
-undump=xelatex
"\newif\ifen\newif\ifja\entrue\jatrue \PassOptionsToClass[target=print]{multilangmanual}\input{$basename}"
```

Press OK to close the configuration window, close the options, select the new typesetting
tool and press the green play button. Most likely MikTeX will take longer for the first
time as it will install required packages on the fly. The pdf should open.

You can compile ``.pdf`` files with one language only by removing ``\entrue`` or ``\jatrue``
in the last line of the configuration. Also, change to ``target=phone`` if you want a
``.pdf`` that is easy to read on a smartphone.

#### Kile

On Linux, you should install the latest [TexLive](https://www.tug.org/texlive/). If you use a different editor than [Kile](https://kile.sourceforge.io/), check the documentation
on how to setup custom typesetting. In particular you need to find the fieldcode
that will be replaced by the filename.

- Navigate to Config Kile -> Tools -> Build -> New
- Choose a name
- Select ``<Custom>``
- set the following settings
    ```
    Command: latexmk
	Options: -pdf -f -interaction=nonstopmode -synctex=1 -jobname=%S -xelatex "\newif\ifen\newif\ifja\entrue\jatrue \PassOptionsToClass{target=print}{multilangmanual}\input{%S.tex}"
	Advanced: 	Run Outside of Kile
		Class: Compile
		Source: tex
		Target: pdf
    ```

#### Bash (Linux command line)

Make sure TexLive is installed. The ``multilangmanual.cls`` file has to be present
in the same folder as the ``.tex`` file, in the local ``texmf`` tree or the system
wide ``texmf`` tree. You can compile with the following command:

``latexmk -pdf -f -jobname=output_en -xelatex "\newif\ifen\newif\ifja\entrue\jatrue \PassOptionsToClass{target=print}{multilangmanual} \input{manual.tex}"``

Replace the ``output_en`` with the desired filename for the ``.pdf``, and ``manual.tex``
with the manual source file.

This command is also used in the continuous integration to compile all manuals
when a new version is pushed.



### Continuous integration

For the continuous integration, the above bash command has to be triggered for
each of the source files and output formats in an environment that has access
to the git repository and a texlive installation.

GitLab has option to setup [gitlab-runner](https://docs.gitlab.com/runner/) on
self-hosted community installations. The following content should act as a
starting point to make an individual configuration, including optional copying
of the compiled ``.pdf`` files to an
[owncloud](https://owncloud.org)/[nextcloud](https://nextcloud.com) share for
distribution to the audience.

Disclaimer: Probably this file can be refactored to write everything in a
programmatic way with loops. But writing all commands verbose does not do any
harm.

The lines after ``script:`` will be consecutively executed in a shell environment.
Lines starting with ``#`` are comments and ignored. 

```
build:
  script:
	# Copy the .cls file from the repository in case it changed
    - cp LatexClass/tex/latex/multilangmanual.cls /home/gitlab-runner/.texmf/tex/latex/multilangmanual.cls
	# We will pass the GITCOMMIT to the multilangmanual class so it is shown
	# in the footer of the pdf. This helps to track the version.
    - GITCOMMIT=`git rev-parse --short HEAD`
	# Clean the output directories
    - rm /home/gitlab-runner/Manualbuild/english/print/*
    - rm /home/gitlab-runner/Manualbuild/english/phone/*
    - rm /home/gitlab-runner/Manualbuild/japanese/print/*
    - rm /home/gitlab-runner/Manualbuild/japanese/phone/*
    - rm /home/gitlab-runner/Manualbuild/multilang/print/*
    - rm /home/gitlab-runner/Manualbuild/multilang/phone/*

	# Specify the name of the first manual (without the .tex extension). The
	# .tex file should be in a folder with the same name.
    - FNAME="example"
    - cd "$FNAME"
    - latexmk -f -jobname="$FNAME"_en -xelatex -interaction=nonstopmode "\newif\ifen\newif\ifja\entrue \PassOptionsToClass{target=print,gitcommit=$GITCOMMIT}{multilangmanual} \input{"$FNAME".tex}"
    - mv -f "$FNAME"_en.pdf /home/gitlab-runner/Manualbuild/english/print/
    - latexmk -f -jobname="$FNAME"_en -xelatex -interaction=nonstopmode "\newif\ifen\newif\ifja\entrue \PassOptionsToClass{target=phone,gitcommit=$GITCOMMIT}{multilangmanual} \input{"$FNAME".tex}"
    - mv -f "$FNAME"_en.pdf /home/gitlab-runner/Manualbuild/english/phone/
    - latexmk -f -jobname="$FNAME"_ja -xelatex -interaction=nonstopmode "\newif\ifen\newif\ifja\jatrue \PassOptionsToClass{target=print,gitcommit=$GITCOMMIT}{multilangmanual} \input{"$FNAME".tex}"
    - mv -f "$FNAME"_ja.pdf /home/gitlab-runner/Manualbuild/japanese/print/
    - latexmk -f -jobname="$FNAME"_ja -xelatex -interaction=nonstopmode "\newif\ifen\newif\ifja\jatrue \PassOptionsToClass{target=phone,gitcommit=$GITCOMMIT}{multilangmanual} \input{"$FNAME".tex}"
    - mv -f "$FNAME"_ja.pdf /home/gitlab-runner/Manualbuild/japanese/phone/
    - latexmk -f -jobname="$FNAME" -xelatex -interaction=nonstopmode "\newif\ifen\newif\ifja\jatrue\entrue \PassOptionsToClass{target=print,gitcommit=$GITCOMMIT,multilang=true}{multilangmanual} \input{"$FNAME".tex}"
    - mv -f "$FNAME".pdf /home/gitlab-runner/Manualbuild/multilang/print/
    - latexmk -f -jobname="$FNAME" -xelatex -interaction=nonstopmode "\newif\ifen\newif\ifja\jatrue\entrue \PassOptionsToClass{target=phone,gitcommit=$GITCOMMIT,multilang=true}{multilangmanual} \input{"$FNAME".tex}"
    - mv -f "$FNAME".pdf /home/gitlab-runner/Manualbuild/multilang/phone/
```



## How to interact with contributors?

Contributors (adding content or translating) are required to edit ``.tex``
files. At least in an university environment, this should not be too much to
ask.

From experience, it has shown beneficial to make a custom, portable MikTex
installation that has the compile configuration already pre-configured, and
distribute it along with the source folder (containing the ``.tex`` file and
images).

The ``Instructions`` folder in this repository contains an ``Contributor.odt``
document that can be used as template for making an instruction sheet for
contributors.

The received files are then locally copied into the (up to date) git folder, and
merged with the master repository. In rare cases, conflicts have to be resolved
manually. Consult the git documentation.



## Note on fonts

[IPA font](https://ipafont.ipa.go.jp/old/#en) is a freely available
font that renders English alongside Japanese with a very appealing
balance. Make sure it is available to the latex installation. On
Windows, add it to the Fonts folder. On linux, add the ``.ttf``
files system wide or for the user.
