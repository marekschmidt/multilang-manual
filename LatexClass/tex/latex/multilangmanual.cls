% multilangmanual.cls
% Class for typesetting multilingual manual
% --- Class structure: identification part
% ---
\ProvidesClass{multilangmanual}[2018/07/03 version 1.0 multilang-manual]
\NeedsTeXFormat{LaTeX2e}

%\RequirePackage{kvoptions-patch}
\RequirePackage{kvoptions}  % options
\RequirePackage{pdftexcmds} % string comparison

% Required code for kvoptions
\SetupKeyvalOptions{
   family=GFM,
   prefix=GFM@
}

% --- Options that can be passed to class
\DeclareStringOption[print]{target} % print, phone
\DeclareStringOption[0]{index} % The number of the manual
\DeclareStringOption[\--]{gitcommit} % Git commit hash (short)
\DeclareStringOption[false]{multilang} % Make multilang file

% --- Options processing
\DeclareDefaultOption{\PassOptionsToClass{\CurrentOption}{scrartcl}}

\ProcessKeyvalOptions*

%%%%%%%%%%%%%%%%%%%%%%%
%% Pass special option to class depending on target
%% Here we pass a6paper to scrartcl in case the target is phone
%% For print we don't have to pass anything
\ifnum\pdf@strcmp{\GFM@target}{phone}=0%
\PassOptionsToClass{a6paper}{scrartcl}
\PassOptionsToClass{singleside}{scrartcl}
\fi
\ifnum\pdf@strcmp{\GFM@target}{print}=0%
\PassOptionsToClass{twoside}{scrartcl}
\fi

%%
%%%%%%%%%%%%%%%%%%%%%%%

% --- Calling the underlying class
% ---
\LoadClass{scrartcl}


% --- Package loading for all targets
\RequirePackage{xeCJK}
\RequirePackage{fancyhdr}
\RequirePackage{amsmath}
\RequirePackage{color}
\RequirePackage{graphicx}
\RequirePackage{tikz}
\RequirePackage{siunitx}
\RequirePackage{parskip} % fix indentation
\sisetup{detect-all}

\newcommand{\subfigimg}[3][,]{%
  \setbox1=\hbox{\includegraphics[#1]{#3}}% Store image in box
  \leavevmode\rlap{\usebox1}% Print image
  \rlap{\hspace*{2pt}\raisebox{\dimexpr\ht1-\baselineskip}%
  {{\colorbox{white}{\makebox[0.8em]{\small\textbf{#2}}}}
  }}% Print label
  \phantom{\usebox1}% Insert appropriate spcing
}

% For date format YYYY-MM-DD
\usepackage[yyyymmdd]{datetime}
\renewcommand{\dateseparator}{--}
\date{}

% For multiple levels for enumerate numbering
\newcommand\theeenumi{\ifnum\value{subsection} > 0 \thesubsection\else\thesection\fi}

\renewcommand\theenumi{\theeenumi.\arabic{enumi}}
\renewcommand\labelenumi{\theenumi}
\renewcommand\theenumii{\theenumi.\alph{enumii}}
\renewcommand\labelenumii{\theenumii}


\renewcommand\p@enumii{}

% For no pagebreaks in (multiline) headings

\newenvironment{absolutelynopagebreak}
  {\par\nobreak\vfil\penalty500\vfilneg% <-- Note
  %Note: The effect of the penalty value is not clear. If using 0 as suggested
  % in https://tex.stackexchange.com/questions/94699/absolutely-definitely-preventing-page-break,
  % Sometimes the heading is broken to a new page while leaving more than
  % 50% of the previous page empty. 500 does not seem to cause any bad
  % behavior.
   \vtop\bgroup}
  {\par\xdef\tpd{\the\prevdepth}\egroup
   \prevdepth=\tpd}

\let\oldsection\section
\let\oldsubsection\subsection

\renewcommand{\section}[1]{%
    \begin{absolutelynopagebreak}
        \oldsection{#1}
    \end{absolutelynopagebreak}
}
\renewcommand{\subsection}[1]{%
    \begin{absolutelynopagebreak}
        \oldsubsection{#1}
    \end{absolutelynopagebreak}
}


% For pdf bookmarks and crossref links inside the document
\usepackage[xetex,bookmarks=true,colorlinks=true,linktoc=section,]{hyperref}
\PassOptionsToPackage{unicode}{hyperref}
\DeclareUrlCommand\email{\urlstyle{rm}}

% --- Common Commands
\setCJKmainfont{IPAexMincho} % for \rmfamily
\setCJKsansfont{IPAexGothic} % for \sffamily
\setCJKmonofont{IPAexGothic}



% Scale down images when exceeding the linewidth
% This is important for target=phone
\newsavebox\IBox
\let\IncludeGraphics\includegraphics
\renewcommand\includegraphics[2][]{%
  \sbox\IBox{\IncludeGraphics[#1]{#2}}%
  \ifdim\wd\IBox>\linewidth\resizebox{\linewidth}{!}{\usebox\IBox}\else\usebox\IBox\fi}



%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%  MAIN LOOP TO DISTINGUISH target=print or target=phone
%%
%% Note  It might be possible to refactor the code to remove some
%% redundant code, but this is not a priority
%%
\ifnum\pdf@strcmp{\GFM@target}{print}=0%
%%
%% Now in Print
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\PassOptionsToClass{twopage}{scrartcl}

% --- Set margins (print)
\areaset[15mm]{15.5cm}{23cm}

% --- Header and footer (print)
\setlength{\fboxrule}{3pt}
\headheight=45.5pt
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\fancyhead{}
\fancyfoot{}
\fancyhead[LO,RE]{\raisebox{-0.5\height}{\includegraphics[height=1.3cm]{Logo}}}
\fancyhead[CO,CE]{{\framebox[1.05\width]{\large\textsf{\ multilang-manual\ }}}}
\fancyhead[RO,LE]{\texttt{Manual No. \num[minimum-integer-digits = 2]{\GFM@index}\--\thepage}}
\fancyfoot[LO,RE]{\scriptsize{\today\quad(\texttt{\GFM@gitcommit})}}
\fancyfoot[RO,LE]{\ifja{\Huge{\textsf{J}}}\else\fi\ifen{\Huge{\textsf{E}}}\else\fi}


% This code will be called after \begin{document} (print)
\AtBeginDocument{
%\maketitle
%\vspace{-180pt}




% --- Index label marks (print)
% These marks are shown on the first page and are two horizontal lines that
% will help to position index sticky tags The index has to be defined in
% the main .tex file before like so
%\def\index{25}

\begin{tikzpicture}[overlay, remember picture,shift={(current page.south west)}]
 \pgfmathsetmacro\ya{29.7 - ((Mod(\GFM@index-1,10)+1)*0.6) - ((((Mod(\GFM@index-1,10)+1)-1)*(2.3))};
 \pgfmathsetmacro\yb{29.7 - ((Mod(\GFM@index-1,10)+1)*(0.6+2.3))};
\begin{scope}[color=lightgray,line width=0.25pt]
  \draw (20cm,\ya cm) -- (21cm,\ya cm);
  \draw (20cm,\yb cm) -- (21cm,\yb cm);
\end{scope}
\end{tikzpicture}


% For Logo and header text
\thispagestyle{fancy}
}



%%%%%%%%%%%%%%%%%%%%
%%
%% Target loop: Phone
%%
\else\ifnum\pdf@strcmp{\GFM@target}{phone}=0%
%%
%%%%%%%%%%%%%%%%%%%%

% Sans serif font better readable on phone
\renewcommand*\familydefault{\sfdefault}

% We don't need large margin on phone
\usepackage[margin=5mm]{geometry}

% Header dimensions have to be adjusted to the small paper size not to take up
% too much space. Also control the bottom and top margin.

\setlength{\fboxrule}{3pt}
\headheight=45.5pt
\footskip=5mm
\textheight=130mm
\headsep=2mm
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\fancyhead{}
\fancyfoot{}
\fancyhead[LO,RE]{\raisebox{-0.5\height}{\includegraphics[height=0.5cm]{Logo}}}
\fancyhead[CO,CE]{{\framebox[1.05\width]{\tiny\textsf{\ multilang-manual\ }}}}
\fancyhead[RO,LE]{\texttt{Manual No. \num[minimum-integer-digits = 2]{\GFM@index}\--\thepage}}
\fancyfoot[LO,RE]{\scriptsize{\today\quad(\texttt{\GFM@gitcommit})}}

\fancyfoot[RO,LE]{\ifja{{\textsf{J}}}\else\fi\ifen{{\textsf{E}}}\else\fi}


% Call this after \begin{document} (phone)
\AtBeginDocument{%
%\maketitle
%\vspace{-50pt}
\thispagestyle{fancy}
}%


%%%%%%%%%%%%%%%%%%%%
%%
%% End of target loop
%%
\fi\fi
%%
%%%%%%%%%%%%%%%%%%%%




% --- Some special code for multilingual compilation
% Japanese and English are shown together, with the English in blue
% There has to be some distinction between the environment where we
% currently are in.
\ifnum\pdf@strcmp{\GFM@multilang}{true}=0%

% Helpercommand to test if we are in caption of a float
\newcommand\InFloat[2]{\@ifundefined{@captype}{#2}{#1}}
% Helpercommand to test if we are in a certain environment
\protected\def\ifenv#1{
   \def\@tempa{#1}%
   \ifx\@tempa\@currenvir
      \expandafter\@firstoftwo
    \else
      \expandafter\@secondoftwo
   \fi
}
% This is the separator between the different languages
\newcommand{\langsep}{
\\}

% Here we replace the \langen from the .tex document
% to include separator and color
% Limitation: We cannot swap the order (e.g. E before J)
\newrobustcmd{\langen}[1]{%
\ifenv{enumerate}{\langsep}{%
	\ifenv{itemize}{\langsep}{%
		\InFloat{}{%
			\ifenv{document}{\langsep}{}
		}%
	}%
}%
{\color{blue}#1}
}%

\fi% end of loop for multilingual 



% Finally, we'll use \endinput to indicate that LaTeX can stop reading this
% file. LaTeX will ignore anything after this line.
\endinput
